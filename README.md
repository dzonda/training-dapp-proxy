# Training dApp Proxy

## Compile

JSLigo:

```bash
npm run compile:ligo
```

Archetype:

```bash
npm run compile:archetype
```

## Deploy

Archetype:

```bash
completium-cli deploy contracts/proxy.arl --as alice --parameters '{"governance": "<tz1>"}'
```

Replace `<tz1>` with your address.
